# fullaunch

A minimalist, simple fullscreen (or windowed) menu/launcher in CrSFML with controller/joystick support

## Prerequisites

* Crystal 1.14
* SFML 1.6.1

## Installation

### Development

```
shards install
shards build
```

### Release

```
shards build --production
sudo cp bin/fullaunch /usr/local/bin
sudo cp bin/fullaunch-windowed /usr/local/bin
sudo cp bin/fullaunch-eventviewer /usr/local/bin
```

## Usage

Create a configuration in `~/.fullaunch/config.yml`, use `config.yml.example` as template. Then use `fullaunch` (or `bin/fullaunch` without installation) to run the fullscreen launcher or `fullaunch-windowed` (or `bin/fullaunch-windowed` without installation) to run the windowed launcher. Use `fullaunch-eventviewer` (or `bin/fullaunch-eventviewer` without installation) to inspect SFML events for joystick bindings configuration.

## Development

* Clone the repository
* `shards install`
* Make changes
* Test manually (I do not plan to add specs, because I don't know how to test a graphical program like this, but I'm open to suggestions)
* Run `bin/ameba` and make sure it passes
* Follow the Git workflow from here

### Code style

The main design principle is SOLID and service object pattern defined as such:
* The name of each class should reflect what it does e.g. `Runner`, `Renderer` etc.
* The classes are grouped into modules/directories representing the domain e.g. `MainWindow`, `Config` etc.
* Parameters for the operation are passed in the class initializer and saved as instance variables
* The instance method `call` is where the main logic lives
* You can use helper methods, but try not to adhere to the single responsibility rule where possible

## Contributing

1. Fork it (<https://gitlab.com/Phitherek_/fullaunch/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [Phitherek_](https://gitlab.com/Phitherek_) - creator and maintainer
