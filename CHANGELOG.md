# CHANGELOG
## v. 1.1.0
* Feature: Background scaling
## v. 1.0.0
* Feature: Texture cache
* Feature: New menu rendering algorithm, so it makes more sense with icons
* Feature: Icon support
## v. 0.2.0
* Feature: Background image
## v. 0.1.1
* Bugfix: Arrow color now matches text color instead of being default white
## v. 0.1.0
* Initial version, basic functionality complete