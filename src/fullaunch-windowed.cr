require "./fullaunch/*"

config = Fullaunch::Config::Retriever.new.call
window = Fullaunch::StandardWindow::Initializer.new("Fullaunch", config[:window_size][:width], config[:window_size][:height]).call
font = Fullaunch::Font::FromFontconfigRetriever.new(config[:font_name], config[:font_style]).call

if font.nil?
  STDERR.puts "Could not find font with name: " + config[:font_name] + ", style: " + config[:font_style]
  exit(1)
end

initial_state = {selected_menu_item: 0, command: nil.as(String?)}

Fullaunch::Texture::FromConfigLoader.new(config).call
Fullaunch::MainLoop::Runner.new(window, font, config, initial_state).call
