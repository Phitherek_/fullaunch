require "crsfml"
require "../video_mode/retriever"

module Fullaunch
  module StandardWindow
    class Initializer
      def initialize(title : String, width : Int32, height : Int32)
        @title = title
        @width = width
        @height = height
      end

      def call
        video_modes = Fullaunch::VideoMode::Retriever.new.call
        x_position = (video_modes.first.width/2 - @width/2).to_i
        y_position = (video_modes.first.height/2 - @height/2).to_i
        window = SF::RenderWindow.new(SF::VideoMode.new(@width, @height), @title, SF::Style::Close)
        window.position = SF.vector2(x_position, y_position)
        window.vertical_sync_enabled = true
        window
      end
    end
  end
end
