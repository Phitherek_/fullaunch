require "../texture/getter"

module Fullaunch
  module Background
    class Renderer
      def initialize(window : SF::RenderWindow, path : String?)
        @window = window
        @path = path
      end

      def call
        return if @path.nil?

        texture = Texture::Getter.new(@path).call
        return if texture.nil?

        sprite = SF::Sprite.new(texture.as(SF::Texture))
        texture_size = texture.size
        window_size = @window.size
        sprite.set_scale(window_size.x / texture_size.x, window_size.y / texture_size.y)
        @window.draw(sprite)
      end
    end
  end
end
