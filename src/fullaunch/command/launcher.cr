module Fullaunch
  module Command
    class Launcher
      def initialize(window : SF::RenderWindow, command : String?)
        @window = window
        @command = command
      end

      def call
        return if @command.nil?

        case @command
        when "$exit$"
          @window.close
        else
          Process.run(@command.as(String), nil, nil, false, true, Process::Redirect::Close, Process::Redirect::Inherit, Process::Redirect::Inherit)
        end
      end
    end
  end
end
