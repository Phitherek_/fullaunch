require "crsfml"

module Fullaunch
  module Font
    class FromFontconfigRetriever
      def initialize(font_name : String, style : String)
        @font_name = font_name
        @style = style
      end

      def call
        file = nil
        font_data = `fc-list "#{@font_name}"`
        font_data.split("\n").each do |data_line|
          data = data_line.split(":")
          if data.last.split("=").last.split(",").includes?(@style)
            file = data.first
            break
          end
        end
        return if file.nil?

        SF::Font.from_file(file)
      end
    end
  end
end
