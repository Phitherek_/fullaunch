require "crsfml"
require "../video_mode/retriever"

module Fullaunch
  module FullscreenWindow
    class Initializer
      def initialize(title : String)
        @title = title
      end

      def call
        video_modes = Fullaunch::VideoMode::Retriever.new.call
        window = SF::RenderWindow.new(video_modes.first, @title, SF::Style::None)
        window.position = SF.vector2(0, 0)
        window.vertical_sync_enabled = true
        window
      end
    end
  end
end
