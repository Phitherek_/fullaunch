require "crsfml"

module Fullaunch
  module EventDisplayWindow
    class Initializer
      def initialize(title : String)
        @title = title
      end

      def call
        window = SF::RenderWindow.new(SF::VideoMode.new(640, 480), @title)
        window.vertical_sync_enabled = true
        window
      end
    end
  end
end
