require "../data/texture_cache"

module Fullaunch
  module Texture
    class Getter
      def initialize(path : String?)
        @path = path
      end

      def call
        return if @path.nil?

        Data::TextureCache.get(@path.as(String))
      end
    end
  end
end
