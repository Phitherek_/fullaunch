require "../data/texture_cache"
require "../types"

module Fullaunch
  module Texture
    class FromConfigLoader
      def initialize(config : ConfigTuple)
        @config = config
      end

      def call
        texture_paths = [] of String?
        texture_paths << @config[:background_image]
        @config[:menu].each do |menu_entry|
          texture_paths << menu_entry[:icon]
        end
        texture_paths.compact!
        texture_paths.each do |texture_path|
          Data::TextureCache.load(texture_path.as(String))
        end
      end
    end
  end
end
