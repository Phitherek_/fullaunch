require "crsfml"

module Fullaunch
  module VideoMode
    class Retriever
      def call
        SF::VideoMode.fullscreen_modes.select do |mode|
          mode.width / 16 * 9 == mode.height || mode.width / 16 * 10 == mode.height || mode.width / 4 * 3 == mode.height
        end
      end
    end
  end
end
