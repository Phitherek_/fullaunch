require "crsfml"
require "../types"

module Fullaunch
  module Menu
    class SfmlItemBuilder
      def initialize(menu_config : Array(Fullaunch::MenuConfigTuple), text_color : Fullaunch::ColorTuple, font : SF::Font, text_size : Int32)
        @menu_config = menu_config
        @text_color = text_color
        @font = font
        @text_size = text_size
      end

      def call
        items = [] of NamedTuple(text: SF::Text, icon_sprite: SF::Sprite?)
        @menu_config.each do |menu_item_config|
          text = SF::Text.new
          text.font = @font
          text.character_size = @text_size
          text.color = SF::Color.new(@text_color[:r], @text_color[:g], @text_color[:b], @text_color[:a])
          text.string = menu_item_config[:label]
          if menu_item_config[:icon].nil?
            items << {text: text, icon_sprite: nil}
            next
          end
          icon_texture = Fullaunch::Texture::Getter.new(menu_item_config[:icon]).call
          if icon_texture.nil?
            items << {text: text, icon_sprite: nil}
            next
          end
          icon_sprite = SF::Sprite.new(icon_texture)
          items << {text: text, icon_sprite: icon_sprite}
        end
        items
      end
    end
  end
end
