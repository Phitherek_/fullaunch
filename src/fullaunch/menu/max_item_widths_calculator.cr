require "crsfml"

module Fullaunch
  module Menu
    class MaxItemWidthsCalculator
      def initialize(menu_items : Array(NamedTuple(text: SF::Text, icon_sprite: SF::Sprite?)))
        @menu_items = menu_items
      end

      def call
        max_item_widths = {full: -1.0, text: -1.0, icon_sprite: -1.0}.to_h
        @menu_items.each do |menu_item|
          text_width = calculate_text_width(menu_item[:text])
          if menu_item[:icon_sprite].nil?
            max_item_widths[:text] = text_width if max_item_widths[:text] < text_width
            max_item_widths[:full] = text_width if max_item_widths[:full] < text_width
            next
          end
          sprite_texture_size = menu_item[:icon_sprite].as(SF::Sprite).texture.as(SF::Texture).size
          text_height = menu_item[:text].font.as(SF::Font).get_line_spacing(menu_item[:text].character_size)
          scale_factor = text_height / sprite_texture_size.y
          sprite_width = sprite_texture_size.x * scale_factor
          final_width = text_width + 20 + sprite_width
          max_item_widths[:text] = text_width if max_item_widths[:text] < text_width
          max_item_widths[:icon_sprite] = sprite_width if max_item_widths[:icon_sprite] < sprite_width
          max_item_widths[:full] = final_width if max_item_widths[:full] < final_width
        end
        max_item_widths
      end

      def calculate_text_width(text : SF::Text)
        text.find_character_pos(text.string.size).x - text.find_character_pos(0).x
      end
    end
  end
end
