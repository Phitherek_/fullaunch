require "../types"
require "./sfml_item_builder"
require "./max_item_widths_calculator"

module Fullaunch
  module Menu
    class Renderer
      def initialize(menu_config : Array(Fullaunch::MenuConfigTuple), window : SF::RenderWindow, text_color : Fullaunch::ColorTuple, font : SF::Font, text_size : Int32, gap_size : Int32, selected_menu_item : Int32)
        @menu_config = menu_config
        @window = window
        @text_color = text_color
        @font = font
        @text_size = text_size
        @gap_size = gap_size
        @selected_menu_item = selected_menu_item
      end

      def call
        window_size = @window.size
        text_height = @font.get_line_spacing(@text_size)
        current_item = @selected_menu_item
        menu_items = Fullaunch::Menu::SfmlItemBuilder.new(@menu_config, @text_color, @font, @text_size).call
        max_item_widths = Fullaunch::Menu::MaxItemWidthsCalculator.new(menu_items).call
        item_start_x = window_size.x/2 - max_item_widths[:full]/2
        item_text_start_x = item_start_x + max_item_widths[:icon_sprite] + 20 if max_item_widths[:icon_sprite] > -1
        item_text_start_x ||= item_start_x
        current_y = window_size.y/2
        item = menu_items[current_item]
        current_menu_items = [] of NamedTuple(text: SF::Text, icon_sprite: SF::Sprite?)
        current_menu_items << draw_item(@window, @font, @text_size, item, item_start_x, item_text_start_x, current_y)
        while current_item - 1 >= 0 && current_y - text_height - @gap_size >= 0
          current_item -= 1
          current_y = current_y - text_height - @gap_size
          item = menu_items[current_item]
          current_menu_items << draw_item(@window, @font, @text_size, item, item_start_x, item_text_start_x, current_y)
        end
        current_y = window_size.y/2
        current_item = @selected_menu_item
        while current_item + 1 < @menu_config.size && current_y + text_height + @gap_size < window_size.y
          current_item += 1
          current_y = current_y + text_height + @gap_size
          item = menu_items[current_item]
          current_menu_items << draw_item(@window, @font, @text_size, item, item_start_x, item_text_start_x, current_y)
        end
        arrow = SF::CircleShape.new(@text_size/3, 3)
        arrow.fill_color = SF::Color.new(@text_color[:r], @text_color[:g], @text_color[:b], @text_color[:a])
        arrow.outline_color = SF::Color.new(@text_color[:r], @text_color[:g], @text_color[:b], @text_color[:a])
        arrow.rotation = 90
        arrow_bounds = arrow.global_bounds
        arrow_x = item_start_x - 20
        arrow_y = window_size.y/2 - arrow_bounds.height/2
        arrow.position = SF.vector2f(arrow_x, arrow_y)
        @window.draw(arrow)
      end

      def draw_item(window : SF::RenderWindow, font : SF::Font, size : Int32, item : NamedTuple(text: SF::Text, icon_sprite: SF::Sprite?), start_x : Float64, text_start_x : Float64, y : Float64)
        text_height = font.get_line_spacing(size)
        real_y = y - (text_height/2)
        item[:text].position = SF.vector2(text_start_x, real_y)
        window.draw(item[:text])
        return item if item[:icon_sprite].nil?

        sprite_texture_size = item[:icon_sprite].as(SF::Sprite).texture.as(SF::Texture).size
        scale_factor = text_height / sprite_texture_size.y
        item[:icon_sprite].as(SF::Sprite).set_scale(scale_factor, scale_factor)
        item[:icon_sprite].as(SF::Sprite).position = SF.vector2(start_x, real_y)
        window.draw(item[:icon_sprite].as(SF::Sprite))
        item
      end

      def calculate_text_width(text : SF::Text)
        text.find_character_pos(text.string.size).x - text.find_character_pos(0).x
      end
    end
  end
end
