require "crsfml"

module Fullaunch
  module Data
    class TextureCache
      @@textures = {} of String => SF::Texture

      def self.load(path : String)
        @@textures[path] = SF::Texture.from_file(path)
      end

      def self.get(path : String)
        @@textures[path]
      end
    end
  end
end
