require "crsfml"
require "../types"

module Fullaunch
  module Event
    class Handler
      JOYSTICK_AXES = {
        "X"    => SF::Joystick::Axis::X,
        "Y"    => SF::Joystick::Axis::Y,
        "Z"    => SF::Joystick::Axis::Z,
        "R"    => SF::Joystick::Axis::R,
        "U"    => SF::Joystick::Axis::U,
        "V"    => SF::Joystick::Axis::V,
        "PovX" => SF::Joystick::Axis::PovX,
        "PovY" => SF::Joystick::Axis::PovY,
      }

      def initialize(window : SF::Window, state : Hash(Symbol, Int32 | String | Nil), menu_config : Array(Fullaunch::MenuConfigTuple), joystick_bindings_config : Array(NamedTuple(up: NamedTuple(button: Int32?, axis: String?, threshold: Float64?), down: NamedTuple(button: Int32?, axis: String?, threshold: Float64?), confirm: NamedTuple(button: Int32?, axis: String?, threshold: Float64?))))
        @window = window
        @state = state
        @menu_config = menu_config
        @joystick_bindings_config = joystick_bindings_config
      end

      def call # ameba:disable Metrics/CyclomaticComplexity
        while event = @window.poll_event
          case event
          when SF::Event::Closed
            @window.close
          when SF::Event::KeyPressed
            case event.code
            when SF::Keyboard::Key::Enter
              @state = select_menu_item(@state, @menu_config)
            when SF::Keyboard::Key::Up
              @state = menu_up(@state, @menu_config)
            when SF::Keyboard::Key::Down
              @state = menu_down(@state, @menu_config)
            when SF::Keyboard::Key::Escape
              @window.close
            end
          when SF::Event::JoystickButtonPressed
            next if event.joystick_id >= @joystick_bindings_config.size
            bindings = @joystick_bindings_config[event.joystick_id]
            case event.button
            when bindings[:confirm][:button]
              @state = select_menu_item(@state, @menu_config)
            when bindings[:up][:button]
              @state = menu_up(@state, @menu_config)
            when bindings[:down][:button]
              @state = menu_down(@state, @menu_config)
            end
          when SF::Event::JoystickMoved
            next if event.joystick_id >= @joystick_bindings_config.size
            bindings = @joystick_bindings_config[event.joystick_id]
            if check_joystick_axis(bindings[:confirm], event)
              @state = select_menu_item(@state, @menu_config)
            elsif check_joystick_axis(bindings[:up], event)
              @state = menu_up(@state, @menu_config)
            elsif check_joystick_axis(bindings[:down], event)
              @state = menu_down(@state, @menu_config)
            end
          end
        end
        @state
      end

      def menu_up(state : Hash(Symbol, Int32 | String | Nil), menu_config : Array(NamedTuple(label: String, command: String?, icon: String?)))
        if state[:selected_menu_item].as(Int32) > 0
          state[:selected_menu_item] = state[:selected_menu_item].as(Int32) - 1
        else
          state[:selected_menu_item] = menu_config.size - 1
        end
        state
      end

      def menu_down(state : Hash(Symbol, Int32 | String | Nil), menu_config : Array(NamedTuple(label: String, command: String?, icon: String?)))
        if state[:selected_menu_item].as(Int32) < menu_config.size - 1
          state[:selected_menu_item] = state[:selected_menu_item].as(Int32) + 1
        else
          state[:selected_menu_item] = 0
        end
        state
      end

      def select_menu_item(state : Hash(Symbol, Int32 | String | Nil), menu_config : Array(NamedTuple(label: String, command: String?, icon: String?)))
        state[:command] = menu_config[state[:selected_menu_item].as(Int32)][:command]
        state
      end

      def check_joystick_axis(binding : NamedTuple(button: Int32?, axis: String?, threshold: Float64?), event : SF::Event::JoystickMoved)
        return false if binding[:axis].nil? || binding[:threshold].nil?

        (event.axis == JOYSTICK_AXES[binding[:axis]] && ((binding[:threshold].as(Float64) < 0) ? event.position <= binding[:threshold].as(Float64) : event.position >= binding[:threshold].as(Float64)))
      end
    end
  end
end
