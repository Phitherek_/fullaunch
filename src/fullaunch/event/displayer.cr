require "crsfml"

module Fullaunch
  module Event
    class Displayer
      def initialize(window : SF::Window)
        @window = window
      end

      def call
        while event = @window.poll_event
          print "Received event: "
          event.inspect(STDOUT)
          puts
          @window.close if event.is_a?(SF::Event::Closed)
        end
      end
    end
  end
end
