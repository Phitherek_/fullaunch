require "yaml"
require "../types"

module Fullaunch
  module Config
    class Retriever
      def call # ameba:disable Metrics/CyclomaticComplexity
        default_config = {
          font_name:        "DejaVu Sans",
          font_style:       "Book",
          text_size:        30,
          text_color:       {r: 255, g: 255, b: 255, a: 255},
          background_color: {r: 0, g: 0, b: 0, a: 255},
          background_image: nil,
          menu:             [
            {label: "Exit", command: "$exit$", icon: nil}.as(Fullaunch::MenuConfigTuple),
          ],
          gap_size:          10,
          joystick_bindings: [] of Fullaunch::JoystickBindingsTuple,
          window_size:       {
            width:  1280,
            height: 720,
          },
        }
        homedir = ENV["HOME"]
        config_path = File.join(homedir, ".fullaunch", "config.yml")
        return default_config if !File.exists?(config_path)

        user_config = File.open(config_path) do |file|
          YAML.parse(file)
        end.as_h

        menu_config = user_config["menu"].as_a? if user_config.has_key?("menu")
        text_color_config = user_config["text_color"].as_h? if user_config.has_key?("text_color")
        background_color_config = user_config["background_color"].as_h? if user_config.has_key?("background_color")
        joystick_bindings_config = user_config["joystick_bindings"].as_a? if user_config.has_key?("joystick_bindings")
        window_size_config = user_config["window_size"].as_h? if user_config.has_key?("window_size")
        {
          font_name:        (user_config.has_key?("font_name") ? user_config["font_name"].as_s? : nil) || default_config[:font_name],
          font_style:       (user_config.has_key?("font_style") ? user_config["font_style"].as_s? : nil) || default_config[:font_style],
          text_size:        (user_config.has_key?("text_size") ? user_config["text_size"].as_i? : nil) || default_config[:text_size],
          text_color:       text_color_config.nil? ? default_config[:text_color] : {r: text_color_config["r"].as_i, g: text_color_config["g"].as_i, b: text_color_config["b"].as_i, a: text_color_config["a"].as_i},
          background_color: background_color_config.nil? ? default_config[:background_color] : {r: background_color_config["r"].as_i, g: background_color_config["g"].as_i, b: background_color_config["b"].as_i, a: background_color_config["a"].as_i},
          background_image: (user_config.has_key?("background_image") ? user_config["background_image"].as_s? : nil) || default_config[:background_image],
          menu:             menu_config.nil? ? default_config[:menu] : menu_config.map do |menu_item|
            {label: menu_item["label"].as_s, command: menu_item.as_h.has_key?("command") ? menu_item["command"].as_s? : nil, icon: menu_item.as_h.has_key?("icon") ? menu_item["icon"].as_s? : nil}
          end,
          gap_size:          (user_config.has_key?("gap_size") ? user_config["gap_size"].as_i? : nil) || default_config[:gap_size],
          joystick_bindings: joystick_bindings_config.nil? ? default_config[:joystick_bindings] : joystick_bindings_config.map do |joystick_binding|
            {
              up:      (!joystick_binding.as_h.has_key?("up") || joystick_binding["up"].nil?) ? {button: nil, axis: nil, threshold: nil} : {button: joystick_binding["up"].as_h.has_key?("button") ? joystick_binding["up"]["button"].as_i? : nil, axis: joystick_binding["up"].as_h.has_key?("axis") ? joystick_binding["up"]["axis"].as_s? : nil, threshold: joystick_binding["up"].as_h.has_key?("threshold") ? joystick_binding["up"]["threshold"].as_f? : nil},
              down:    (!joystick_binding.as_h.has_key?("down") || joystick_binding["down"].nil?) ? {button: nil, axis: nil, threshold: nil} : {button: joystick_binding["down"].as_h.has_key?("button") ? joystick_binding["down"]["button"].as_i? : nil, axis: joystick_binding["down"].as_h.has_key?("axis") ? joystick_binding["down"]["axis"].as_s? : nil, threshold: joystick_binding["down"].as_h.has_key?("threshold") ? joystick_binding["down"]["threshold"].as_f? : nil},
              confirm: (!joystick_binding.as_h.has_key?("confirm") || joystick_binding["confirm"].nil?) ? {button: nil, axis: nil, threshold: nil} : {button: joystick_binding["confirm"].as_h.has_key?("button") ? joystick_binding["confirm"]["button"].as_i? : nil, axis: joystick_binding["confirm"].as_h.has_key?("axis") ? joystick_binding["confirm"]["axis"].as_s? : nil, threshold: joystick_binding["confirm"].as_h.has_key?("threshold") ? joystick_binding["confirm"]["threshold"].as_f? : nil},
            }
          end,
          window_size: window_size_config.nil? ? default_config[:window_size] : {width: window_size_config["width"].as_i, height: window_size_config["height"].as_i},
        }
      end
    end
  end
end
