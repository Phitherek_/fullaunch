require "crsfml"
require "../event/displayer"
require "../font/from_fontconfig_retriever"
require "../menu/renderer"

module Fullaunch
  module EventDisplayLoop
    class Runner
      def initialize(window : SF::RenderWindow, font : SF::Font)
        @window = window
        @font = font
      end

      def call
        while @window.open?
          Fullaunch::Event::Displayer.new(@window).call
          @window.clear(SF::Color::Black)
          text = SF::Text.new
          text.font = @font
          text.character_size = 20
          text.color = SF::Color::White
          text.string = "The events are logged to the console. Close this window to finish."
          @window.draw(text)
          @window.display
        end
      end
    end
  end
end
