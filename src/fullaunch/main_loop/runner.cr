require "crsfml"
require "../types"
require "../event/handler"
require "../font/from_fontconfig_retriever"
require "../menu/renderer"
require "../background/renderer"

module Fullaunch
  module MainLoop
    class Runner
      @state : Hash(Symbol, Int32 | String | Nil)

      def initialize(window : SF::RenderWindow, font : SF::Font, config : Fullaunch::ConfigTuple, initial_state : NamedTuple(selected_menu_item: Int32, command: String?))
        @window = window
        @font = font
        @config = config
        @state = initial_state.to_h
      end

      def call
        while @window.open?
          @state = Fullaunch::Event::Handler.new(@window, @state, @config[:menu], @config[:joystick_bindings]).call
          Fullaunch::Command::Launcher.new(@window, @state[:command].as(String?)).call
          @state[:command] = nil
          @window.request_focus
          @window.clear(SF::Color.new(@config[:background_color][:r], @config[:background_color][:g], @config[:background_color][:b], @config[:background_color][:a]))
          Fullaunch::Background::Renderer.new(@window, @config[:background_image]).call
          Fullaunch::Menu::Renderer.new(@config[:menu], @window, @config[:text_color], @font, @config[:text_size], @config[:gap_size], @state[:selected_menu_item].as(Int32)).call
          @window.display
        end
      end
    end
  end
end
