require "./fullaunch/event_display_window"
require "./fullaunch/config"
require "./fullaunch/font"
require "./fullaunch/event_display_loop"
require "./fullaunch/event"

window = Fullaunch::EventDisplayWindow::Initializer.new("Event Viewer").call
config = Fullaunch::Config::Retriever.new.call
font = Fullaunch::Font::FromFontconfigRetriever.new(config[:font_name], config[:font_style]).call

if font.nil?
  STDERR.puts "Could not find font with name: " + config[:font_name] + ", style: " + config[:font_style]
  exit(1)
end

Fullaunch::EventDisplayLoop::Runner.new(window, font).call
